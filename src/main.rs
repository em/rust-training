use libc::timeval;

fn main() -> Result<(), anyhow::Error> {
    let mut tp = timeval{tv_usec:0, tv_sec:0};
    let ret = unsafe {
        libc::gettimeofday(&mut tp, std::ptr::null_mut())
    };
    anyhow::ensure!(ret == 0, "failed to get the time");
    let time: f64 = tp.tv_sec as f64 + tp.tv_usec as f64 / 1_000_000.0;
    println!("time: {:?}", time);
    Ok(())
}
